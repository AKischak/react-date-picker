import data from './api_response.json';

class ApiService {

	getCalendarData() {
		return Promise.resolve(data);
	}

}

export default ApiService;
