import React, { Component } from 'react';
import DaySelect from '../DaySelect';
import DateRange from '../DatePicker/DateRange';
import moment from 'moment';

import ApiService from '../../data/ApiService';

class CustomerOrder extends Component {

	constructor(props) {
		super(props);

		this.service = new ApiService();

		this.state = {
			initialized: false,
			daysConfig: {},
            dayList: [],
			userSelect: [],
			dateRange: {
				from: moment().startOf('month'),
				to: moment().startOf('month').add(2, 'month').subtract(1, 'day')
			},
			rangeSelectHelper: {
				currentMode: 'recursive',
				single: {},
				recursive: {
					"monday": {
						selected: false
					},
                    "thursday": {
                        selected: false
                    }
				}
			}
		};
	}

	day2IndexConvert(key){
		let dayMap = {
			'monday': 1,
			'tuesday': 2,
			'wednesday': 3,
			'thursday': 4,
			'friday': 5,
			'saturday': 6,
			'sunday': 7,
		};
		if(typeof key === 'string' && dayMap.hasOwnProperty(key.toLowerCase())){
            return dayMap[key.toLowerCase()];
		} else {
			for(let day in dayMap){
				if(dayMap[day] === key){
					return day;
				}
			}
		}
		return -1;
	}

	initDayConfig(data) {
		let daysConfig = {};
		data['categorizeds'].forEach(({category}) => {
			let dayKey = this.day2IndexConvert(category.name);

			if(category.type === 5){
                daysConfig[dayKey] = category;
			}
		});

		this.setState({ dayList: this.calculateDayList({ daysConfig }), daysConfig, initialized: true });
	}

	isDayOff(date, cutOffConf){
		if(!cutOffConf) return false;

		let cutOff = date.clone().isoWeekday(this.day2IndexConvert(cutOffConf.cutoff_day));
        let time = moment(cutOffConf.cutoff_time, "HH:mm:ss");

		cutOff.set('hour', time.get('hour'));
		cutOff.set('minute', time.get('minute'));
		cutOff.set('second', time.get('second'));

		return moment().isAfter(cutOff);
	}

	calculateDayList(updated){
		let state = Object.assign({}, this.state, updated);
		let {rangeSelectHelper, dateRange, userSelect, daysConfig} = state;

        let days = dateRange.to.diff(dateRange.from, 'days');
        let dayList = [];
        for(let i = 0; i < days; i++){
            let date = dateRange.from.clone().add(i, 'day');
            let dayOfWeek = date.isoWeekday();
			let dayOfWeekStr = this.day2IndexConvert(dayOfWeek);

            if(daysConfig.hasOwnProperty(dayOfWeek)){
                let dayConf = daysConfig[dayOfWeek];
                let isDayCutOff = this.isDayOff(date, dayConf.delivery_day);

                let isDaySelected = userSelect.indexOf(date.format('YYYY-MM-DD')) >= 0;
				if(rangeSelectHelper.currentMode === 'recursive'){
					if(rangeSelectHelper.recursive[dayOfWeekStr].selected){
						isDaySelected = true;
					}
				}

				dayList.push({
                    date,
                    cutOff: isDayCutOff,
                    isDaySelected: isDaySelected
                })
            }
        }
        return dayList;
	}

	refreshDayList(userSelect) {
        userSelect = userSelect || this.state.userSelect;
		this.setState({dayList: this.calculateDayList({userSelect}), userSelect});
	}

	componentWillMount() {
		this.service.getCalendarData().then((data) => {
			this.initDayConfig(data);
		});
	}

	getPreloader() {
		return (
			<span>Data loafing...</span>
		);
	}

    onDayClick(momentDate){
		let dateStr = momentDate.format('YYYY-MM-DD');
        let newUserSelect = this.state.userSelect;

		if(this.state.userSelect.indexOf(dateStr) < 0){
            newUserSelect = [].concat(this.state.userSelect, dateStr);
		} else {
			newUserSelect = newUserSelect.filter((x) => x !== dateStr);
		}
		this.setState({ newUserSelect, dayList: this.calculateDayList({userSelect: newUserSelect}) });
	}

    handlePeriodHelperSelect(){
    	let day = 'monday';
    	let { rangeSelectHelper } = this.state;
    	if(rangeSelectHelper.currentMode === 'recursive'){
    		rangeSelectHelper.recursive[day].selected = !rangeSelectHelper.recursive[day].selected;
    		this.setState({ dayList: this.calculateDayList({ rangeSelectHelper }) });
		}
	}

	getView() {

		let dateRangeProps = {
			startDate: null,
			endDate: null,
            minDate: this.state.dateRange.from,
            maxDate: this.state.dateRange.to,
            linkedCalendars: true,
            disableOutOfDate: true,
            dayList: this.state.dayList,
            onSingleSelect: this.onDayClick.bind(this)
		};

		return (
			<div>
				<button onClick={this.handlePeriodHelperSelect.bind(this)}>Select all mondays</button>

				<DaySelect/>
				<DateRange {...dateRangeProps} onlyClasses={true}/>
			</div>
		);
	}

	render() {
		return (
			<div>
				{this.state.initialized ? this.getView() : this.getPreloader()}
			</div>
		);
	}

}

export default CustomerOrder;
