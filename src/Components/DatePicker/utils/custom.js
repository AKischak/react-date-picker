export function isOutOfRange(min, max, date){
    if((min && min.isAfter(date)) || (max && max.isBefore(date))){
        return true;
    }
    return false
}
